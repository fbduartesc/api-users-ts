import express, { Request, Response } from 'express'
import { User } from '../../models/user'

const router = express.Router()

router.get('/api/users', async (req: Request, res: Response) => {
  const users = await User.find({})
  return res.status(200).send(users)
})

router.get('/api/users/:id', async (req: Request, res: Response) => {
  const user = await User.findOne({_id: req.params.id})
  return res.status(200).send(user)
})

router.delete('/api/users/:id', async (req: Request, res: Response) => {
  const user = await User.findByIdAndRemove(req.params.id)
  return res.status(200).send(user)
})

router.put('/api/users/:id', async (req: Request, res: Response) => {
  const { name, login, email } = req.body;

  const user = User.build({ name, login, email })
  await user.update({_id: req.params.id})
  return res.status(201).send(user)
})

router.post('/api/users', async (req: Request, res: Response) => {
  const { name, login, email } = req.body;

  const user = User.build({ name, login, email })
  await user.save()
  return res.status(201).send(user)
})

export { router as userRouter }