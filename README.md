<div align="center">  
  <h1>Express | NodeJS | MongoDB | TypeScript</h1>

 :rocket: *API: this project will build API using typescript, nodejs, express and mongodb to create, change, delete and read users in the database*
  </div>


# :pushpin: Table of contents

- [Technologies](#computer-technologies)
- [How to run](#construction_worker-how-to-run)
- [License](#closed_book-license)

# :computer: Technologies

This project was made using the following technologies:

<ul>
  <li><a href="https://nodejs.org/en/">NodeJS</a></li>
  <li><a href="https://expressjs.com/pt-br/">Express</a></li>
  <li><a href="https://www.typescriptlang.org/">TypeScript</a></li>
  <li><a href="https://www.mongodb.com/">MongoDB</a></li>
</ul>

# :construction_worker: How to run

### :computer: Downloading project 

```bash
# Clone repository into your machine
$ git clone https://gitlab.com/fbduartesc/api-users-ts.git
```

### :computer: Installation 

```bash
# Install dependencies
$ npm install
```

### 💻 Running project on a web browser

```bash
# Run the command, to start the application
$ npm start
```

# :closed_book: License

Released in 2020.

Made with passion by [Fabio Duarte de Souza](https://gitlab.com/fbduartesc) 🚀.
This project is under the [MIT license](https://gitlab.com/fbduartesc/api-users-ts/blob/master/LICENSE).