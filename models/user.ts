import mongoose from 'mongoose'

interface IUser {
  name: string;
  login: string;
  email: string;
}

interface userModelInterface extends mongoose.Model<UserDoc> {
  build(attr: IUser): UserDoc
}

interface UserDoc extends mongoose.Document {
  name: string;
  login: string;
  email: string;
}

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  login: {
    type: String, 
    required: true
  },
  email: {
    type: String,
    required: true
  }
})

userSchema.statics.build = (attr: IUser) => {
  return new User(attr)
}

const User = mongoose.model<UserDoc, userModelInterface>('User', userSchema)

User.build({
  name: 'Usuário 123',
  login: 'user123',
  email: 'user@user.com'
})

export { User }