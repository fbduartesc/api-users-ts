import express from 'express';
import mongoose from 'mongoose'
import { json } from 'body-parser';
import { userRouter } from './routes/user'

const app = express()
app.use(json())
app.use(userRouter)

mongoose.connect('mongodb://localhost:27017/users', {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
}, () => {
  console.log('Conectado no banco de dados')
})

app.listen(3000, () => {
  console.log('Servidor rodando na porta 3000')
})